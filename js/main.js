let body = document.querySelector('.js-body');
let navbarMenuDropdownItems = document.querySelectorAll('.js-navbar-dropdown-item');
let navbarMenuDropdownTopLevelItems = document.querySelectorAll('.js-navbar-dropdown-top-level');
let headerSearchToggleBtn = document.querySelector('.js-header-search-toggle');
let headerSearchCloseBtn = document.querySelector('.js-header-search-close');
let headerSearchForm = document.querySelector('.js-header-search');
let navbarMenu = document.querySelector('.js-navbar-menu');
let navbarMenuToggle = document.querySelector('.js-navbar-menu-toggle');

// ===== Modified Bootstrap accordion =====
$('.w-accordion__item-title').click(function () {

    const $this = $(this);
    const $next = $this.next();

    if ($next.hasClass('collapsing')) return;

    $next.collapse('toggle');
    $this.toggleClass('collapsed');
});

lightGallery(document.querySelector('.lightgallery'), {
    selector: '.js-lightgallery-item',
    speed: 250,
    backdropDuration: 150,
    download: false
});

// ===== DARKEN PAGE =====

navbarMenuDropdownTopLevelItems.forEach(el => {
    el.addEventListener('mouseenter', e => {
        body.classList.add('backdrop');
    })
    el.addEventListener('mouseleave', e => {
        body.classList.remove('backdrop');
    })
})


// ===== MENU FUNCTIONALITY =====
navbarMenuDropdownItems.forEach(el => {
    el.addEventListener('click', e => {
        e.stopPropagation();
        if (e.target.classList.contains('extended')) {
            e.target.classList.remove('extended');
        } else {
            e.target.classList.add('extended');
        }
    })
})
// console.log(navbarMenuDropdownLinks);
// navbarMenuDropdownLinks.forEach(el => {
//     el.addEventListener('click', e => {
//
//         console.log(el);
//         const parent = e.target.parentElement;
//         if(parent.classList.contains('extended')) {
//             parent.classList.remove('extendend');
//         }
//         else {
//             parent.classList.add('extendend');
//         }
//     })
// })

// ===== Jidelnicek carousel =====
$(document).ready(function () {
    $('.js-meals-carousel').slick({
        infinite: false,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        swipeToSlide: false,
        swipe: false,
        speed: 300
    });
});


// ===== CLNDR Calendar =====
$('.js-aside-calendar').clndr({
    formatWeekdayHeader: function (day) {
        return day.format('dd');
    },
});

// ===== Searchbar Function =====
if (headerSearchToggleBtn && headerSearchCloseBtn) {
    headerSearchToggleBtn.addEventListener('click', () => {
        toggleSearchbar();
    })

    headerSearchCloseBtn.addEventListener('click', () => {
        toggleSearchbar();
    })
}

function toggleSearchbar() {
    if (!headerSearchForm.classList.contains('active')) {
        headerSearchForm.classList.add('active');
    } else {
        headerSearchForm.classList.remove('active');
    }
}

// ===== Navbar menu hamburger =====
if (navbarMenuToggle && navbarMenu) {
    navbarMenuToggle.addEventListener('click', () => {
        toggleNavbar();
    })
}

function toggleNavbar() {
    if (!navbarMenu.classList.contains('active')) {
        navbarMenu.classList.add('active');
        navbarMenuToggle.classList.add('open');
        console.log('Opening menu');
        return;
    }

    navbarMenu.classList.remove('active');
    navbarMenuToggle.classList.remove('open');
    console.log('Closing menu');
}











