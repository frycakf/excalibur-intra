# Starter

## Instalace projektu
1. naklonujte si repozitář do cílové složky
2. v package.json přejmenujte *starter* na název projektu (název, popis, BS proxy)
3. WAMP server
	1. vytvořte virtual host v httpd-vhosts.conf
	2. vytvořte virtual host v C:/Windows/System32/drivers/etc/hosts
	3. restartujte Apache (nebo celý WAMP Server)
4. nainstalovat NPM balíčky příkazem `npm install`
5. spusťte watch příkazem `npm start`

## Příkazy
- `npm start` / `npm run start` - spustí browser-sync a watch CSS kódu
- `npm run devserver` - spustí browser-sync
- `npm run watch:css` - spustí CSS watch (compile)
- `npm run build:css` - produkční CSS (compile → concat → prefix → compress)